\contentsline {chapter}{\numberline {I}PENDAHULUAN}{1}% 
\contentsline {section}{\numberline {1.1}Latar Belakang}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {paragraph}{}{1}% 
\contentsline {section}{\numberline {1.2}Identifikasi Masalah}{2}% 
\contentsline {section}{\numberline {1.3}Tujuan dan Manfaat}{2}% 
\contentsline {subsection}{\numberline {1.3.1}Tujuan}{2}% 
\contentsline {subsection}{\numberline {1.3.2}Manfaat}{2}% 
\contentsline {section}{\numberline {1.4}Ruang Lingkup}{3}% 
\contentsline {chapter}{\numberline {II}LANDASAN TEORI}{4}% 
\contentsline {section}{\numberline {2.1}Deskripsi Topik Yang Sama}{4}% 
\contentsline {subsection}{\numberline {2.1.1}Arsitektur \textit {Microservice} untuk Resiliensi Sistem Informasi}{4}% 
\contentsline {paragraph}{}{4}% 
\contentsline {subsection}{\numberline {2.1.2}Implementasi Teknologi Mikro \textit {service} pada Pengembangan \textit {Mobile Learning} }{4}% 
\contentsline {paragraph}{}{4}% 
\contentsline {subsection}{\numberline {2.1.3}Peningkatan Ketersediaan Aplikasi Web Menggunakan Arsitektur Layanan Mikro Berdasarkan Identifikasi Log Akses}{5}% 
\contentsline {paragraph}{}{5}% 
\contentsline {section}{\numberline {2.2}Deskripsi Metode Yang Sama}{5}% 
\contentsline {subsection}{\numberline {2.2.1}Pengujian Sistem Informasi Penjualan dan Persediaan Barang Dengan \textit {Web Application Load Stress and Performance Testing} (WAPT)}{5}% 
\contentsline {paragraph}{}{5}% 
\contentsline {subsection}{\numberline {2.2.2}\textit {Software Testing} Pengujian Performansi dan Tingkat \textit {Stress} Pada \textit {Website} Ekspedisi JNE dan TIKI}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {subsection}{\numberline {2.2.3}Penerapan Metode Waterfall dalam Sistem Informasi Bank Sampah berbasis Web}{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {section}{\numberline {2.3}Teori Umum}{6}% 
\contentsline {subsection}{\numberline {2.3.1} \textit {Microservices} }{6}% 
\contentsline {paragraph}{}{6}% 
\contentsline {subsection}{\numberline {2.3.2} \textit {Frontend} }{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {subsection}{\numberline {2.3.3} \textit {Stress Testing}}{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {subsection}{\numberline {2.3.4}\textit {Perfomance Testing}}{7}% 
\contentsline {paragraph}{}{7}% 
\contentsline {chapter}{\numberline {III}ANALISIS ORGANISASI PERUSAHAAN}{9}% 
\contentsline {section}{\numberline {3.1}Sejarah Perusahaan}{9}% 
\contentsline {paragraph}{}{9}% 
\contentsline {section}{\numberline {3.2}Visi dan Misi Perusahaan}{9}% 
\contentsline {subsection}{\numberline {3.2.1}Visi}{9}% 
\contentsline {paragraph}{}{9}% 
\contentsline {subsection}{\numberline {3.2.2}Misi}{10}% 
\contentsline {section}{\numberline {3.3}Deskripsi dan Ruang Lingkup \textit {Internship} }{10}% 
\contentsline {paragraph}{}{10}% 
\contentsline {chapter}{\numberline {IV}METODOLOGI PENELITIAN}{11}% 
\contentsline {section}{\numberline {4.1}Diagram Alur Metodologi Penelitian}{11}% 
\contentsline {paragraph}{}{11}% 
\contentsline {paragraph}{}{11}% 
\contentsline {section}{\numberline {4.2}Tahapan - Tahapan Diagram Alur Metodologi}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {subsection}{\numberline {4.2.1} \textit {Planning}}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {subsection}{\numberline {4.2.2} \textit {Design}}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {subsection}{\numberline {4.2.3} \textit {Coding}}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {subsection}{\numberline {4.2.4} \textit {Testing}}{12}% 
\contentsline {paragraph}{}{12}% 
\contentsline {chapter}{\numberline {V}ANALISIS DAN PERANCANGAN}{13}% 
\contentsline {section}{\numberline {5.1}Analisis}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {subsection}{\numberline {5.1.1}Analisis Arsitektur yang Sedang Berjalan}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {subsection}{\numberline {5.1.2}Analisis Arsitektur yang Akan Dibangun}{13}% 
\contentsline {paragraph}{}{13}% 
\contentsline {subsection}{\numberline {5.1.3}Analisis Kebutuhan Aplikasi}{14}% 
\contentsline {paragraph}{}{14}% 
\contentsline {section}{\numberline {5.2}Perancangan UML}{15}% 
\contentsline {subsection}{\numberline {5.2.1}Usecase Diagram}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsubsection}{\numberline {5.2.1.1}Definisi Aktor}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsubsection}{\numberline {5.2.1.2}Definisi Usecase}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsubsection}{\numberline {5.2.1.3}Skenario Usecase}{15}% 
\contentsline {paragraph}{}{15}% 
\contentsline {subsection}{\numberline {5.2.2}Class Diagram}{18}% 
\contentsline {paragraph}{}{18}% 
\contentsline {subsection}{\numberline {5.2.3}Sequence Diagram}{18}% 
\contentsline {paragraph}{}{18}% 
\contentsline {subsubsection}{\numberline {5.2.3.1}Sequence Diagram Melihat Halaman Home}{18}% 
\contentsline {paragraph}{}{18}% 
\contentsline {subsubsection}{\numberline {5.2.3.2}Sequence Diagram Melihat Halaman Produk}{19}% 
\contentsline {paragraph}{}{19}% 
\contentsline {subsubsection}{\numberline {5.2.3.3}Sequence Diagram Melihat Halaman Fitur}{19}% 
\contentsline {paragraph}{}{19}% 
\contentsline {subsubsection}{\numberline {5.2.3.4}Sequence Diagram Melihat Halaman Knowledge Base}{19}% 
\contentsline {paragraph}{}{19}% 
\contentsline {subsubsection}{\numberline {5.2.3.5}Sequence Diagram Melihat Halaman Kontak}{19}% 
\contentsline {paragraph}{}{19}% 
\contentsline {subsection}{\numberline {5.2.4}Activity Diagram}{20}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{\numberline {5.2.4.1}Activity Diagram Melihat Halaman Home}{20}% 
\contentsline {paragraph}{}{20}% 
\contentsline {subsubsection}{\numberline {5.2.4.2}Activity Diagram Melihat Halaman Produk}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsubsection}{\numberline {5.2.4.3}Activity Diagram Melihat Halaman Fitur}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsubsection}{\numberline {5.2.4.4}Activity Diagram Melihat Halaman Kontak}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsection}{\numberline {5.2.5}Collaboration Diagram}{21}% 
\contentsline {paragraph}{}{21}% 
\contentsline {subsubsection}{\numberline {5.2.5.1}Collaboration Diagram Melihat Halaman Home}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {subsubsection}{\numberline {5.2.5.2}Collaboration Diagram Melihat Halaman Prouk}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {subsubsection}{\numberline {5.2.5.3}Collaboration Diagram Melihat Halaman Fitur}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {subsubsection}{\numberline {5.2.5.4}Collaboration Diagram Melihat Halaman \textit {Knowledge Base}}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {subsubsection}{\numberline {5.2.5.5}Collaboration Diagram Melihat Halaman Kontak}{22}% 
\contentsline {paragraph}{}{22}% 
\contentsline {subsection}{\numberline {5.2.6}Statechart Diagram}{23}% 
\contentsline {paragraph}{}{23}% 
\contentsline {subsubsection}{\numberline {5.2.6.1}Statechart Diagram Melihat Halaman Home}{23}% 
\contentsline {paragraph}{}{23}% 
\contentsline {subsubsection}{\numberline {5.2.6.2}Statechart Diagram Melihat Halaman Produk}{23}% 
\contentsline {paragraph}{}{23}% 
\contentsline {subsubsection}{\numberline {5.2.6.3}Statechart Diagram Melihat Halaman Fitur}{23}% 
\contentsline {paragraph}{}{23}% 
\contentsline {subsubsection}{\numberline {5.2.6.4}Statechart Diagram Melihat Halaman \textit {Knowledge Base}}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsubsection}{\numberline {5.2.6.5}Statechart Diagram Melihat Halaman Kontak}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsection}{\numberline {5.2.7}Perancangan Antar Muka Sistem}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsubsection}{\numberline {5.2.7.1}Perancangan Antar Muka Halaman Home}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsubsection}{\numberline {5.2.7.2}Perancangan Antar Muka Halaman Produk}{24}% 
\contentsline {paragraph}{}{24}% 
\contentsline {subsubsection}{\numberline {5.2.7.3}Perancangan Antar Muka Halaman Fitur}{25}% 
\contentsline {paragraph}{}{25}% 
\contentsline {subsubsection}{\numberline {5.2.7.4}Perancangan Antar Muka Halaman \textit {Knowledge Base}}{25}% 
\contentsline {paragraph}{}{25}% 
\contentsline {subsubsection}{\numberline {5.2.7.5}Perancangan Antar Muka Halaman Kontak}{25}% 
\contentsline {paragraph}{}{25}% 
\contentsline {chapter}{\numberline {VI}PENGKAJIAN DAN EVALUASI}{35}% 
\contentsline {section}{\numberline {6.1}Lingkungan Pengkajian}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {subsection}{\numberline {6.1.1}Kebutuhan Perangkat Keras}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {subsection}{\numberline {6.1.2}Kebutuhan Perangkat Lunak}{35}% 
\contentsline {paragraph}{}{35}% 
\contentsline {section}{\numberline {6.2}Pengkajian}{36}% 
\contentsline {paragraph}{}{36}% 
\contentsline {subsection}{\numberline {6.2.1}\textit {Planning}}{36}% 
\contentsline {paragraph}{}{36}% 
\contentsline {paragraph}{}{36}% 
\contentsline {subsection}{\numberline {6.2.2}Design}{36}% 
\contentsline {paragraph}{}{36}% 
\contentsline {subsection}{\numberline {6.2.3}Coding}{36}% 
\contentsline {paragraph}{}{36}% 
\contentsline {subsection}{\numberline {6.2.4}Testing}{37}% 
\contentsline {paragraph}{}{37}% 
\contentsline {section}{\numberline {6.3}Evaluasi}{37}% 
\contentsline {paragraph}{}{37}% 
\contentsline {subsection}{\numberline {6.3.1}Lingkungan Pengkajian}{38}% 
\contentsline {paragraph}{}{38}% 
\contentsline {subsection}{\numberline {6.3.2}Evaluasi dengan GTMetrix}{39}% 
\contentsline {paragraph}{}{39}% 
\contentsline {subsection}{\numberline {6.3.3}Evaluasi dengan WAPT}{39}% 
\contentsline {paragraph}{}{39}% 
\contentsline {chapter}{\numberline {VII} DISKUSI}{42}% 
\contentsline {section}{\numberline {7.1}Kesimpulan}{42}% 
\contentsline {paragraph}{}{42}% 
\contentsline {section}{\numberline {7.2}Saran}{42}% 
\contentsline {paragraph}{}{42}% 
\contentsline {chapter}{Daftar Pustaka}{43}% 
